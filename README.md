# NARPS


## Getting started

You just need python3, pip and jupyter

> Go and do what's asked on [https://github.com/Inria-Empenn/narps_open_pipelines](https://github.com/Inria-Empenn/narps_open_pipelines)

**N.B** We strongly suggest the use of virtualenv

## Experiment process

1. Generate dockerfile based on matlab mcr or spm12 release you need to test on
2. Run pipeline 2T6S using the command
   > for i in {1..N}; do docker run -it -v /[REPO_PATH]/narps_open_pipelines/:/work/ -v run/reproduced/:[A_DIRECTORY_IN_WHICH_YOU_SAVE_YOUR_RUNS] [DOCKER IMAGE GENERATED] /neurodocker/job.sh 2T6S M;

   **Where N is the number of time you will run a single experiment (batch size), and M the number of subjects to consider in the experiment**

3. Once done, you can analyze the batch data with **Analyze_batch_single_team.ipynb**
4. Repeat from step 1 with other configurations, let it be SPM12, matlab or even hardware variation.
   > **N.B:** ARM hardware is not available through docker as long as it is only available on Apple Sillicon with matlab 2023b and spm12 r7771
   
5. Use **Analyze_2_runs_single_team.ipynb** to compare results between two runs of the same team based on different configurations (resp spm12, matlab, hw)
   
7. Use **Comparisons_niftis.ipynb** to compare results between two runs. It generates :
   
    - Statiscal map images for each hypotheses of each run
    - Statistical map of the difference between two hypotheses when relevant
    - An emphasized difference statistical map when relevant (a diverging pixel is set to 1E2 to emphasize it)
    - A .csv of the dice coefficient of the hypotheses of both runs (semantics)
    - A .csv of the statiscial difference of the 3 types of cuts (sagittal, horizontal, frontal) when there is a structural difference between hypotheses. for each slice in the 3 dimensions, it contains mean, std_dev, min, max, nb_of_points_in_slice, nb_of_points_diverging

8. Use **CSV_Analysis.ipynb** to analyse the csv that were generated previously and observe how important are the differences 


### NARPS.ipynb

A notebook describing the NARPS variability understanding, issues and ideas.

### NARPS_cmp.ipynb

A notebook that explores the comparison of brain statistical maps.

### Docker_gen.ipynb

A notebook that helps the generating of dockerfiles (or even images) encapsulating configurable versions of Matlab or SPM12.

### Comparisons_niftis.ipynb

A notebook used to compare outputs of =/= NARPS pipelines.

### NARPS_VAR_EXPLO.ipynb

A plain text notebook describing the evolution of the experimentation (lab notebook somehow)